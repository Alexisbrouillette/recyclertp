package com.example.recyclertp.ui.fournisseurs;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class FournisseursViewModel extends ViewModel {

    //private MutableLiveData<ArrayList<Fournisseur>> mData;
    public ArrayList<Fournisseur> mData;
    public FournisseursViewModel() {
        //mData = new MutableLiveData<>();
        mData = new ArrayList<Fournisseur>();
    }

    /*public LiveData<ArrayList<Fournisseur>> getMData() {
        return mData;
    }*/

    public void updateData(JSONObject newData)
    {
        try {
            JSONArray jsonArray = newData.getJSONArray("suppliers");

            //boucle pour mettre la donnes dans le mData
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject fournisseurJson = jsonArray.getJSONObject(i);
                Fournisseur fournisseur = new Fournisseur(
                        fournisseurJson.getInt("id"),
                        fournisseurJson.getString("name"),
                        fournisseurJson.getString("address_line_1"),
                        fournisseurJson.getString("address_line_2"),
                        fournisseurJson.getString("address_city"),
                        fournisseurJson.getString("address_province"),
                        fournisseurJson.getString("address_postal_code"),
                        fournisseurJson.getString("telephone"),
                        fournisseurJson.getString("contact"));
                mData.add(fournisseur);
            }
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }


}