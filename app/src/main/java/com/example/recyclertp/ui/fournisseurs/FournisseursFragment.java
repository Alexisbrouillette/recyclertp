package com.example.recyclertp.ui.fournisseurs;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.recyclertp.RecyclerViewAdapterProducts;
import com.example.recyclertp.RecyclerViewAdapterSuppliers;
import com.example.recyclertp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class FournisseursFragment extends Fragment {
    //variable pour le recycler
    private RecyclerViewAdapterSuppliers adapter;

    private FournisseursViewModel fournisseursViewModel;
    private TextView textView;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        fournisseursViewModel =
                ViewModelProviders.of(this).get(FournisseursViewModel.class);
        View root = inflater.inflate(R.layout.fragment_fournisseurs, container, false);

        requeteRetourArrayList("suppliers");

        return root;
    }
    //methode qui faire la requete pour get le liste de suppliers ou products selon le param
    public void requete(final String type)
    {

        RequestQueue queue = Volley.newRequestQueue(getActivity());
        String url ="https://api.myjson.com/bins/wewjb";
        final ArrayList<Fournisseur> data = new ArrayList<Fournisseur>();

        // Request a string response from the provided URL.
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                        //update les donnes des fournisseurs
                        fournisseursViewModel.updateData(response);
                    }

                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error
                        error.printStackTrace();
                    }
                });
        // Add the request to the RequestQueue.
        queue.add(jsonObjectRequest);

    }
}